import React from 'react';
import {Route, Routes} from "react-router-dom";
import HomeScreen from "./Home.screen";
import SearchScreen from "./Search.screen";
import {Ribbon} from "../components/Ribbon";
import {ElementScreenProxy} from "./element";


export default function ScreenRouter() {
    return (
        <div>
            <Ribbon/>
            <Routes>
                <Route path="" element={<HomeScreen/>}/>
                <Route path="/search" element={<SearchScreen/>}/>
                <Route path="/e/:entity/:id" element={<ElementScreenProxy/>}/>
                {/*    <Route path="character/:id" element={<CharacterScreen/>}/>*/}
                {/*</Route>*/}
            </Routes>
        </div>
    );
}
