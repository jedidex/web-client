import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Button, Grid, TextField, Typography} from '@mui/material';
import React, {useState} from 'react';
import {useNavigate} from "react-router-dom";
import {faBookJournalWhills} from '@fortawesome/free-solid-svg-icons'
import {faGitlab} from '@fortawesome/free-brands-svg-icons'

export default function HomeScreen() {

    const [query, setQuery] = useState<string>("")
    const navigate = useNavigate();

    const handleButtonClick = () => {
        navigate({
            pathname: '/search',
            search: `?q=${query}`,
        });
    }

    return (


        <Grid
            style={{
                position: "absolute",
                top: "0",
                bottom: "0",
                right: "0"
            }}
            container
            justifyContent="center"
            alignItems="center"
            spacing={0}
        >
            <Grid container item xs={10} md={6} xl={4} lg={4} spacing={2}
                  alignItems="center" justifyContent={"center"}>
                <Grid container item xs={12} justifyContent="center" alignItems={"end"}>
                    <Grid item>
                        <Typography variant={"h3"}>
                            Jedidex.com
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant={"caption"}>
                            Alpha
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        onKeyDown={(event => {
                            if (event.key === "Enter")
                                handleButtonClick()
                        })}
                        fullWidth
                        value={query}
                        onChange={(e) => setQuery(e.target.value)}
                        placeholder="Darth Vader" variant="outlined"/>
                </Grid>
                <Grid item>
                    <Button onClick={handleButtonClick} fullWidth>
                        Search
                    </Button>
                </Grid>
                <Grid item xs={12}/>
                <Grid item>
                    <a href={"https://gitlab.com/jedidex"} target={"_blank"} rel={"noreferrer"}>
                        <Button color={"secondary"} startIcon={<FontAwesomeIcon icon={faGitlab}/>}>
                            Gitlab
                        </Button>
                    </a>
                </Grid>
                <Grid item>
                    <a href={"https://docs.jedidex.com"}>
                        <Button color={"secondary"} startIcon={<FontAwesomeIcon icon={faBookJournalWhills}/>}>
                            Documentation
                        </Button>
                    </a>
                </Grid>
            </Grid>
        </Grid>


    )

}