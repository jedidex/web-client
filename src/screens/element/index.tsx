import React, {useEffect, useState} from 'react';
import {Routes, useParams} from "react-router-dom";
import CharacterScreen from "./Character.screen";
import {Method, useApi} from "../../hooks/useApi";
import {Character} from "../../model/element/character";
import {SWElement} from "../../model/element";
import {Button, Grid} from "@mui/material";


export default function ElementsRouter() {
    return (
        <Routes>

        </Routes>
    );
}

function Redirect(props: { to: string }) {

    const handleRedirect = () => {
        const wind = window.open(props.to, '_blank')
        if (wind)
            wind.focus()
    }

    return (
        <Grid container justifyContent={"center"}>
            <Grid item>
                <Button onClick={handleRedirect}>
                    Open on wookiepedia
                </Button>
            </Grid>
        </Grid>
    );
}

export function ElementScreenProxy() {
    let {entity, id} = useParams();

    const [requestFeedback, getElement] = useApi<SWElement>(Method.GET, "elements/" + id)

    const [element, setElement] = useState<SWElement>();

    useEffect(() => {
        getElement()
            .then(setElement)
            .catch(reason => console.log(reason))
    }, [getElement])

    if (!element)
        return null
    else if (requestFeedback.isWorking)
        return <>
            Loading
        </>
    switch (entity) {
        case "character":
            return <CharacterScreen character={element as Character}/>
        default:
            return <Redirect to={element.wp}/>
    }
}


