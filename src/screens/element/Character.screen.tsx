import {
    AccordionDetails,
    AccordionSummary,
    Box,
    Button,
    Card,
    CardContent,
    Divider,
    Grid,
    Stack,
    styled,
    Tab,
    Tabs,
    Typography,
    useMediaQuery,
    useTheme
} from '@mui/material';
import MuiAccordion, {AccordionProps} from '@mui/material/Accordion';
import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {Method, useApi} from "../../hooks/useApi";
import {Character} from "../../model/element/character";
import {Masonry} from "@mui/lab";
import {ImageCard} from "../../components/ImageCard";
import SimpleItem from "../../components/SimpleItem";
import {MultipleElementsCard} from "../../components/MultipleElementsCard";
import {SWElement} from "../../model/element";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({theme}) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

function groupByEntity(collection: SWElement[]) {
    let i = 0, val, result: { [key: string]: SWElement[] } = {};

    for (; i < collection.length; i++) {
        val = collection[i].entity;

        if (result[val] === undefined)
            result[val] = []

        result[val].push(collection[i])
    }
    return result;
}

export default function CharacterScreen(props: { character: Character }) {
    let {id} = useParams();
    const [tabIndex, setTabIndex] = useState(0);

    const theme = useTheme();
    const isMediumOrLess = useMediaQuery(theme.breakpoints.down('lg'));


    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setTabIndex(newValue);
    };

    return (
        <Stack>
            <Box
                sx={{bgcolor: 'secondary.dark'}}
                pt={5} pb={5} pr={2} pl={2}>
                <Typography variant={"h3"}>
                    {props.character.name}
                </Typography>
            </Box>
            <Box sx={{bgcolor: 'secondary.main'}} pr={2} pl={2}>
                <Tabs value={tabIndex} onChange={handleChange} centered={isMediumOrLess}>
                    <Tab label="Character"/>
                    <Tab label="Appearances"/>
                </Tabs>
            </Box>
            <Grid container p={2} justifyContent={"center"}>
                <Grid item xs={12} lg={8}>
                    {tabIndex === 0 ?
                        <CharacterDetails character={props.character} isSmallScreen={isMediumOrLess}/>
                        :
                        <CharacterAppearances id={id || ""}/>
                    }
                </Grid>
            </Grid>
        </Stack>
    )

}

function CharacterAppearances(props: { id: string }) {


    const [appearances, setAppearances] = useState<{ [key: string]: SWElement[] }>({});

    const [characterRequest, getAppearances] = useApi<SWElement[]>(Method.GET, "characters/" + props.id + "/appears_in")

    useEffect(() => {
        getAppearances()
            .then(result => {
                const dataGrouped = groupByEntity(result);
                setAppearances(dataGrouped)
            })
            .catch(reason => console.log(reason))
    }, [getAppearances])


    return (
        <div>
            {Object.keys(appearances).map(key => (
                <Accordion key={key}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                    >
                        <Typography>{key}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Grid container spacing={2} alignItems={"stretch"}>
                            {appearances[key].map(element => (
                                <Grid item xs={6} md={4} lg={3} key={element.id}>
                                    <ImageCard alt={element.name}
                                               fullHeight
                                               title={element.name}
                                               image={element.image}>
                                    </ImageCard>
                                </Grid>
                            ))}

                        </Grid>
                    </AccordionDetails>
                </Accordion>
            ))}
        </div>
    )
}

function CharacterDetails(props: { character: Character, isSmallScreen: boolean }) {

    const character = props.character


    return (
        <Masonry columns={props.isSmallScreen ? 1 : 2} spacing={2}>
            <ImageCard alt={character.name}
                       imageWidth={302}
                       title={"About"}
                       image={character.image}>

                <Stack spacing={1}
                       divider={<Divider orientation="horizontal" flexItem/>}
                >
                    <SimpleItem label={"Gender"} value={character.gender}/>
                    <SimpleItem label={"Height"} value={character.height}/>
                    <SimpleItem label={"Weight"} value={character.mass}/>
                    <SimpleItem label={"Eyes color"} value={character.eyes}/>
                    <SimpleItem label={"Hair"} value={character.hair}/>
                    <SimpleItem label={"Skin"} value={character.skin}/>
                </Stack>

            </ImageCard>

            <ImageCard alt={"homeworld"}
                       title={"Homeworld"}
                       image={character.homeworld?.image}>

                <Grid container>
                    <Grid item xs={4}>
                        Location
                    </Grid>
                    <Grid item xs={8}>
                        {character.homeworld?.name}
                    </Grid>
                </Grid>
            </ImageCard>

            <ImageCard alt={"death location"}
                       title={"Death"}
                       image={character.death_location?.image}>

                <Stack spacing={1}
                       divider={<Divider orientation="horizontal" flexItem/>}>
                    <SimpleItem label={"Location"} value={character.death_location?.name}/>
                    <SimpleItem label={"Date"} value={character.death}/>
                </Stack>

            </ImageCard>

            <ImageCard alt={"birth location"}
                       title={"Birth"}
                       image={character.birth_location?.image}>

                <Stack spacing={1}
                       divider={<Divider orientation="horizontal" flexItem/>}>
                    <SimpleItem label={"Location"} value={character.birth_location?.name}/>
                    <SimpleItem label={"Date"} value={character.birth}/>
                </Stack>

            </ImageCard>

            <MultipleElementsCard title={"Apprentices"} elements={character.apprentices}/>

            <MultipleElementsCard title={"Masters"} elements={character.masters}/>

            <Card>
                <CardContent>
                    <Stack>
                        <Typography variant={"h6"} mb={1}>
                            Also referred as
                        </Typography>
                        <Grid container spacing={1}>
                            {character.alternate_names?.map(name => (
                                <Grid key={name} item xs={6}>
                                    {name}
                                </Grid>
                            ))}
                        </Grid>
                    </Stack>
                </CardContent>
            </Card>

            <Card>
                <CardContent>
                    <Stack>
                        <Typography variant={"h6"} mb={1}>
                            Affiliations
                        </Typography>
                        <Stack spacing={1}
                               divider={<Divider orientation="horizontal" flexItem/>}
                        >
                            {character.affiliation?.map(element => (
                                <Box key={element.id}>
                                    {element.name}
                                </Box>
                            ))}
                        </Stack>
                    </Stack>
                </CardContent>
            </Card>

            <Button variant={"outlined"}
                    onClick={() => {
                        const wind = window.open(character.wp, '_blank')
                        if (wind)
                            wind.focus()
                    }}
            >
                More on wookiepedia
            </Button>

        </Masonry>
    )
}