import {
    Avatar,
    Box,
    Button,
    Grid,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    SwipeableDrawer,
    TextField,
    useMediaQuery,
    useTheme
} from '@mui/material';
import React, {useCallback, useEffect, useState} from 'react';
import {Method, useApi} from "../hooks/useApi";
import {PageResponse} from "../model/pageResponse";
import {SWElement} from "../model/element";
import {useNavigate, useSearchParams} from "react-router-dom";
import {supportedElements} from "../utils/constants";
import {EntityPreview} from "../components/entity/EntityPreview";


export default function SearchScreen() {

    const theme = useTheme();
    const isMediumOrLess = useMediaQuery(theme.breakpoints.down('lg'));

    const [searchParams] = useSearchParams();
    const navigate = useNavigate();

    const [results, setResults] = useState<SWElement[]>([])
    const query = searchParams.get("q") || ""
    const [selectedEntity, setSelectedEntity] = useState<SWElement>()

    const [queryText, setQueryText] = useState<string>(searchParams.get("q") || "")

    const [result, performSearch] = useApi<PageResponse>(Method.GET, "elements/search")

    const callbackPerformSearch = useCallback((params: { q: string }) => {
            setResults([])
            performSearch({}, params).then(response => setResults(response.results))
        }, [performSearch]
    )

    useEffect(() => {
        if (!query)
            navigate({
                pathname: '/',
            });

        callbackPerformSearch({q: query})
    }, [query, callbackPerformSearch, navigate])

    useEffect(() => {
        if (results.length > 0 && !isMediumOrLess && supportedElements.includes(results[0].entity))
            setSelectedEntity(results[0])
        else
            setSelectedEntity(undefined)

    }, [results, isMediumOrLess])

    const handleOnSearch = () => {
        navigate({
            pathname: '/search',
            search: `?q=${queryText}`,
        });
    }

    const handleOnSelected = (element: SWElement) => {
        if (!supportedElements.includes(element.entity)) {
            const wind = window.open(element.wp, '_blank')
            if (wind)
                wind.focus()
        } else {
            navigate({
                search: `?q=${queryText}`,
            });

            setSelectedEntity(element)
        }
    }

    return (
        <Grid container>
            <Grid item container spacing={2} xs={12} md={10} lg={8} xl={6} alignItems={"center"}>
                <Grid item xs={10}>
                    <Box p={2}>
                        <TextField
                            onKeyDown={(event => {
                                if (event.key === "Enter")
                                    handleOnSearch()
                            })}
                            fullWidth
                            value={queryText}
                            onChange={(e) => setQueryText(e.target.value)}
                            placeholder="Search" variant="outlined"/>
                    </Box>
                </Grid>
                <Grid item xs={1}>
                    <Button
                        fullWidth
                        onClick={handleOnSearch}>
                        Search
                    </Button>
                </Grid>
            </Grid>
            {result.isWorking && <Grid item xs={12} p={2}>
                Loading
            </Grid>}
            <Grid item xs={12}/>
            <Grid item xs={12} md={8} lg={6} xl={4}>
                <Box p={isMediumOrLess ? 0 : 2}>
                    <List>
                        {results.map(result =>
                            <ListItem
                                button
                                selected={selectedEntity === result}
                                secondaryAction={
                                    !supportedElements.includes(result.entity) && "WP"
                                }
                                key={result.wp}
                                onClick={() => handleOnSelected(result)}>
                                <ListItemIcon>
                                    <Avatar alt={result.name} src={result.image}/>
                                </ListItemIcon>
                                <ListItemText primary={result.name} secondary={result.entity}/>
                            </ListItem>
                        )}
                    </List>
                </Box>
            </Grid>
            {selectedEntity && !isMediumOrLess &&
                <Grid item container lg={6} xl={8} justifyContent={"center"}>
                    <Grid item lg={10} xl={7}>
                        <EntityPreview element={selectedEntity}/>
                    </Grid>

                </Grid>
            }
            {isMediumOrLess &&
                <SwipeableDrawer
                    // style={{backgroundColor: "transparent", backgroundImage: "none"}}
                    PaperProps={{
                        sx: {
                            backgroundColor: "transparent",
                            backgroundImage: "none"
                        }
                    }}
                    anchor={"bottom"}
                    open={!!selectedEntity}
                    onClose={() => setSelectedEntity(undefined)}
                    onOpen={() => {
                    }}
                >
                    <EntityPreview element={selectedEntity}/>
                </SwipeableDrawer>
            }
        </Grid>
    )

}