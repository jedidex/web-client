import React, {createContext, useEffect, useReducer, useRef} from 'react';
import {loadState, saveState} from './utils/localStorage';
import {BrowserRouter} from "react-router-dom";
import {reducer} from "./reducer";
import {darkTheme} from "./utils/theme";
import {CssBaseline, ThemeProvider} from '@mui/material';
import ScreenRouter from "./screens";

export const AppContext = createContext({});

function App() {
    const [state, dispatch] = useReducer(reducer, loadState());

    const isFirstRun = useRef(true);

    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        saveState(state);
    }, [state, state.user])

    return (
        <AppContext.Provider value={[state, dispatch]}>
            <ThemeProvider theme={darkTheme}>
                <CssBaseline/>
                <BrowserRouter>
                    <ScreenRouter/>
                </BrowserRouter>
            </ThemeProvider>
        </AppContext.Provider>
    );
}

export default App;
