import {Box, Card, CardContent, CardMedia, Stack, Typography, useMediaQuery, useTheme} from "@mui/material";
import React from "react";

export function ImageCard(props:
                              {
                                  image?: string,
                                  alt: string,
                                  title?: string,
                                  imageWidth?: number
                                  children?: React.ReactNode,
                                  fullHeight?: boolean
                              }) {

    const theme = useTheme();
    const isMediumOrLess = useMediaQuery(theme.breakpoints.down('lg'));

    let imageWidth = props.imageWidth ? props.imageWidth : 151

    if (isMediumOrLess && imageWidth > 151)
        imageWidth = 151

    return (
        <Card
            sx={{display: 'flex', height: props.fullHeight ? "100%" : undefined}}>
            {props.image &&
                <CardMedia
                    sx={{width: imageWidth}}
                    component="img"
                    alt={props.alt}
                    image={props.image}
                />

            }
            <Box sx={{display: 'flex', flexDirection: 'column', flex: 1}}>
                <CardContent
                    sx={{flex: '1'}}>
                    <Stack spacing={1}>
                        {props.title && <Typography variant={"h6"}>
                            {props.title}
                        </Typography>}
                        {props.children}
                    </Stack>
                </CardContent>
            </Box>
        </Card>
    )
}