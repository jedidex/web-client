import {Grid} from "@mui/material";


export function Ribbon() {

    return (
        <div style={{
            width: "280px",
            padding: "8px",
            // position: "absolute",
            textAlign: "center",
            color: "#f0f0f0",
            top: "40px",
            right: "-80px",
            transform: "rotate(45deg)",
            position: "fixed",
            fontSize: "15px",
            zIndex: 10000,
            backgroundColor: "#fc5949"
        }}>
            <Grid container
                  direction="row"
                  justifyContent="center"
                  alignItems="center">

                <Grid item>
                    under development
                </Grid>
            </Grid>
        </div>
    )
}