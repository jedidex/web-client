import {Avatar, Box, Card, CardContent, Grid, Stack, Typography} from "@mui/material";
import React from "react";
import {SWElement} from "../model/element";
import {Link} from "react-router-dom";

export function MultipleElementsCard(props:
                                         {
                                             title?: string,
                                             elements?: [SWElement],
                                             onClick?: (element: SWElement) => void
                                         }) {

    const handleElementSelected = (element: SWElement) => {
        if (props.onClick)
            props.onClick(element)
    }

    return (
        <Card>
            <CardContent>
                <Stack>
                    {props.title && <Typography pb={2} variant={"h6"}>
                        {props.title}
                    </Typography>}
                    <Grid container
                          spacing={2}
                          alignItems={"stretch"}>
                        {props.elements?.map(element => (
                            <Grid item
                                // onClick={() => handleElementSelected(element)}
                                  component={Link}
                                  to={"/e/" + element.entity.toLowerCase() + "/" + element.id}
                                  key={element.id}
                                  xs={3} lg={2}>
                                <Stack>
                                    <Avatar
                                        sx={{
                                            width: 'auto',
                                            height: '100px'
                                        }}
                                        variant={"rounded"} src={element.image} alt={element.name}/>
                                    <Box
                                        sx={{color: "text.primary"}}
                                        style={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap",
                                            width: "100%"
                                        }}>
                                        <Typography variant={"caption"}>
                                            {element.name}
                                        </Typography>
                                    </Box>

                                </Stack>

                            </Grid>
                        ))}
                    </Grid>
                </Stack>
            </CardContent>
        </Card>
    )
}