import {Grid} from "@mui/material";
import React from "react";

export default function SimpleItem(props: { label: string, value?: string | number }) {
    return (
        <Grid container>
            <Grid item xs={4}>
                {props.label}
            </Grid>
            <Grid item xs={8}>
                {props.value}
            </Grid>
        </Grid>
    )
}