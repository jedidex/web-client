import React from 'react';
import {Grid} from "@mui/material";
import {DataItemContainer, ElementItem} from "../DetailItem";
import {Character} from "../../../model/element/character";


export function CharacterPreview(props: { character: Character | undefined }) {
    const character = props.character

    return (
        <Grid container justifyContent={"start"} spacing={2}>
            <Grid item xs={6}>
                <DataItemContainer label={"Biological"}>
                            <span>
                                {character && character.gender}
                            </span>
                    {character && character.species && <ElementItem element={character.species}/>}
                </DataItemContainer>
            </Grid>

            <Grid item xs={6}>
                <DataItemContainer label={"Homeworld"}>
                    {character && <ElementItem element={character.homeworld}/>}
                </DataItemContainer>
            </Grid>

            <Grid item xs={12} sm={6}>
                <DataItemContainer label={"Birth"}>
                    {character && character.birth_location && <ElementItem element={character.birth_location}/>}
                    <span>
                                {character && character.birth}
                            </span>
                </DataItemContainer>
            </Grid>

            <Grid item xs={12} sm={6}>
                <DataItemContainer label={"Death"}>
                    {character && character.death_location && <ElementItem element={character.death_location}/>}
                    <span>
                                {character && character.death}
                            </span>
                </DataItemContainer>
            </Grid>

            <Grid item xs={6}>
                <DataItemContainer label={"Weight"}>
                    {character && character.mass && <span>{character.mass} Kg</span>}
                </DataItemContainer>
            </Grid>

            <Grid item xs={6}>
                <DataItemContainer label={"Height"}>
                    {character && character.height && <span>{character.height} m</span>}
                </DataItemContainer>
            </Grid>

            <Grid item xs={12}>
                <DataItemContainer label={"Cyber prosthetics"}>
                    {character && character.cyber}
                </DataItemContainer>
            </Grid>

        </Grid>

    )
}