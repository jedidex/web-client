import React, {useEffect, useState} from 'react';
import {CharacterPreview} from "./entity_preview/CharacterPreview";
import {Method, useApi} from "../../hooks/useApi";
import {Character} from "../../model/element/character";
import {ElementType, SWElement} from "../../model/element";
import {Avatar, Button, Divider, Grid, Paper, Skeleton, Typography} from "@mui/material";
import {useNavigate, useSearchParams} from "react-router-dom";


export function EntityPreview(props: { element: SWElement | undefined }) {

    const navigate = useNavigate();
    const [searchParams] = useSearchParams();

    const query = searchParams.get("q") || ""

    const [elementRequest, getElement] = useApi<Character>(Method.GET, "elements/" + props.element?.id)

    const [element, setElement] = useState<SWElement>()

    useEffect(() => {

        setElement(undefined)
        if (props.element)
            getElement()
                .then(setElement)
                .catch(reason => console.log(reason))
    }, [getElement, props.element])

    const getComponent = (type: ElementType, element: SWElement | undefined) => {
        switch (type) {
            default:
            case ElementType.Character:
                return <CharacterPreview character={element as Character}/>
        }
    }

    const handleElementClick = () => {
        if (element)
            navigate({
                pathname: `/e/${element.entity.toLowerCase()}/${element.id}`,
                search: `?q=${query}`
            })
    }

    return (
        <Paper style={{width: "100%", marginTop: 100}}>
            <Grid container>
                <Grid item style={{marginTop: -75}} ml={2}>
                    <Avatar sizes={"largest"}
                            sx={{width: 150, height: 150}}
                            alt={element ? element.name : "Entity avatar"}
                            src={element ? element.image : undefined}
                    />
                </Grid>
                <Grid style={{textAlign: "center", minWidth: "50"}} m={2}>
                    {element ? <Typography variant={"h4"}>
                            {element.name}
                        </Typography> :
                        <Skeleton variant={"text"} width={250} height={50}/>
                    }
                </Grid>
                <Grid item xs={12} mt={2}>
                    <Divider/>
                </Grid>
                <Grid item xs={12} p={2}>
                    {props.element && getComponent(props.element.entity, element)}
                </Grid>
                <Grid item xs={12} p={1} style={{textAlign: "center"}}>
                    <Button
                        onClick={handleElementClick}>
                        More information
                    </Button>
                </Grid>
            </Grid>
        </Paper>
    )


}