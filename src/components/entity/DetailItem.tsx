import {Avatar, Divider, Stack, Typography} from "@mui/material";
import React from 'react';
import {SWElement} from "../../model/element";

export function DataItemContainer(props: { label: string, children?: React.ReactNode }) {
    return (
        <Stack spacing={1}>
            <Typography fontWeight={"bold"}>
                {props.label}
            </Typography>
            <Stack
                style={{minHeight: 25}}
                alignItems={"center"}
                spacing={1}
                direction={"row"}
                divider={<Divider orientation="vertical" flexItem/>}>
                {props.children}
            </Stack>
        </Stack>
    )
}

export function ElementItem(props: { element?: SWElement, elements?: SWElement[], suffix?: string }) {

    let elementList: SWElement[] = []

    if (props.elements)
        elementList = props.elements
    else if (props.element)
        elementList = [props.element]


    return (
        <Stack spacing={1}>
            {elementList.map((element: SWElement) =>
                <Stack key={element.wp} direction={"row"} spacing={2} alignItems={"center"}>
                    {element && element.image &&
                        <Avatar alt={element.name} src={element.image}/>
                    }
                    <span>{element && element.name}</span>
                    {props.suffix && <span>{props.suffix}</span>}
                </Stack>
            )}
        </Stack>
    )
}

