import {ElementType, SWElement} from "../element";

export class Droid extends SWElement
{

birth?: string
death?: string
droid_class?: string
cost?: number
length?: number
width?: number
height?: number
mass?: number
sensor?: [string]
plating?: [string]
equipment?: [string]
gender?: string
_type: ElementType = ElementType.Droid

}
