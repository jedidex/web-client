import {ElementType, SWElement} from "../element";

export class IndividualShip extends SWElement
{

cost?: number
sysmods?: [string]
length?: number
width?: number
height?: number
mass?: number
max_accel?: string
mglt?: string
max_speed?: number
maneuverability?: string
engine?: string
hyperdrive_rating?: [string]
hdrange?: string
poweroutput?: string
power?: [string]
shield_gen?: [string]
hull?: [string]
sensor?: [string]
target?: [string]
navigation?: [string]
avionics?: [string]
maincomp?: [string]
countermeasures?: [string]
armament?: [string]
bays?: string
crew?: string
min_crew?: string
passengers?: string
capacity?: string
cargohandling?: string
consumables?: string
lifesupport?: string
communications?: string
othersystems?: [string]
availability?: string
role?: [string]
commission?: string
destroyed?: string
retired?: string
registry?: string
aliases?: [string]
_type: ElementType = ElementType.IndividualShip

}
