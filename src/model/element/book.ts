import {ElementType, SWElement} from "../element";

export class Book extends SWElement {

    release_date?: string
    media_type?: string
    pages?: number
    isbn?: string
    timeline?: string
    _type: ElementType = ElementType.Book

}
