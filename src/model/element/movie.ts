import {ElementType, SWElement} from "../element";

export class Movie extends SWElement {

    release_date?: string
    runtime?: string
    budget?: string
    language?: string
    timeline?: string
    _type: ElementType = ElementType.Movie

}
