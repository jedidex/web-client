import {ElementType, SWElement} from "../element";

export class Moon extends SWElement {

    coordinates?: string
    lengthday?: string
    lengthyear?: string
    moon_class?: string
    diameter?: number
    atmosphere?: string
    climate?: string
    gravity?: string
    terrain?: [string]
    water?: [string]
    population?: [string]
    demonym?: [string]
    imports?: [string]
    exports?: [string]
    _type: ElementType = ElementType.Moon

}
