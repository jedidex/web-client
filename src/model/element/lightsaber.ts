import {ElementType, SWElement} from "../element";

export class Lightsaber extends SWElement {

    model?: string
    type?: string
    created?: string
    destroyed?: string
    discovered?: string
    hilt_model?: string
    hilt_shape?: string
    hilt_length?: number
    hilt_material?: string
    blade_type?: string
    color?: string
    blade_length?: string
    mods?: string
    weight?: string
    protection?: string
    capacity?: string
    range?: string
    design?: string
    markings?: string
    purpose?: string
    _type: ElementType = ElementType.Lightsaber

}
