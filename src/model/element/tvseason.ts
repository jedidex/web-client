import {ElementType, SWElement} from "../element";

export class TVSeason extends SWElement {

    format?: string
    num_episodes?: number
    runtime?: string
    network?: [string]
    first_aired?: string
    last_aired?: string
    timeline?: string
    _type: ElementType = ElementType.TVSeason

}
