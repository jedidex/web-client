import {ElementType, SWElement} from "../element";

export class Planet extends SWElement {

    position?: string
    coordinates?: string
    xyz?: string
    distance?: string
    lengthday?: string
    lengthyear?: string
    planet_class?: string
    diameter?: number
    atmosphere?: string
    climate?: string
    gravity?: string
    terrain?: [string]
    water?: [string]
    population?: [string]
    demonym?: [string]
    imports?: [string]
    exports?: [string]
    _type: ElementType = ElementType.Planet

}
