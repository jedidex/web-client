import {ElementType, SWElement} from "../element";

export class DroidSeries extends SWElement
{

class?: string
cost?: number
length?: number
width?: number
height?: number
mass?: number
sensor?: [string]
plating?: [string]
equipment?: [string]
gender?: string
first_made?: string
retired?: string
_type: ElementType = ElementType.DroidSeries

}
