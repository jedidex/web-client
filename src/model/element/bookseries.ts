import {ElementType, SWElement} from "../element";

export class BookSeries extends SWElement
{

last_date?: string
first_date?: string
media_type?: [string]
pages?: number
isbn?: string
num_books?: number
timeline?: string
_type: ElementType = ElementType.BookSeries

}
