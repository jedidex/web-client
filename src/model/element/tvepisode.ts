import {ElementType, SWElement} from "../element";

export class TVEpisode extends SWElement {

    episode?: number
    production?: string
    air_date?: string
    runtime?: string
    timeline?: string
    _type: ElementType = ElementType.TVEpisode

}
