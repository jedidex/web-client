import {ElementType, SWElement} from "../element";

export class ComicBook extends SWElement
{

publication_date?: string
pages?: number
upc?: string
issue?: number
timeline?: string
_type: ElementType = ElementType.ComicBook

}
