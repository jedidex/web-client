import {ElementType, SWElement} from "../element";

export class Nebula extends SWElement {

    coordinates?: string
    xyz?: string
    population?: [string]
    imports?: [string]
    exports?: [string]
    distance?: string
    size?: number
    formed?: string
    distinctions?: [string]
    _type: ElementType = ElementType.Nebula

}
