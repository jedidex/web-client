import {ElementType, SWElement} from "../element";

export class Character extends SWElement {

    birth?: string
    death?: string
    gender?: string
    height?: number
    mass?: number
    hair?: string
    eyes?: string
    skin?: string
    cyber?: string
    homeworld?: SWElement
    species?: SWElement
    birth_location?: SWElement
    death_location?: SWElement
    alternate_names?: [string]
    masters?: [SWElement]
    apprentices?: [SWElement]
    affiliation?: [SWElement]

    _type: ElementType = ElementType.Character

}
