import {ElementType, SWElement} from "../element";

export class Deity extends SWElement
{

type?: string
powers?: string
form?: string
gender?: string
first_apparence?: string
_type: ElementType = ElementType.Deity

}
