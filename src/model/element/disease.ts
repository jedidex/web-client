import {ElementType, SWElement} from "../element";

export class Disease extends SWElement
{

date_engineered?: string
number_infected?: [string]
number_killed?: [string]
transmission_type?: [string]
incubation_period?: string
symptoms?: [string]
treatments?: [string]
_type: ElementType = ElementType.Disease

}
