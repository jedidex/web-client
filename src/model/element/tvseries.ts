import {ElementType, SWElement} from "../element";

export class TVSeries extends SWElement {

    format?: string
    num_episodes?: number
    num_seasons?: number
    runtime?: string
    network?: [string]
    first_aired?: string
    last_aired?: string
    timeline?: string
    _type: ElementType = ElementType.TVSeries

}
