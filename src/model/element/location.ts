import {ElementType, SWElement} from "../element";

export class Location extends SWElement {

    created?: string
    destroyed?: string
    terrain?: string
    _type: ElementType = ElementType.Location

}
