import {ElementType, SWElement} from "../element";

export class Constellation extends SWElement {

    coordinates?: string
    xyz?: string
    population?: [string]
    imports?: [string]
    exports?: [string]
    _type: ElementType = ElementType.Constellation

}
