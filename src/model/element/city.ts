import {ElementType, SWElement} from "../element";

export class City extends SWElement
{

constructed?: string
destroyed?: string
rebuilt?: string
continent?: string
climate?: string
population?: string
_type: ElementType = ElementType.City

}
