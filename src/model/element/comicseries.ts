import {ElementType, SWElement} from "../element";

export class ComicSeries extends SWElement
{

start_date?: string
end_date?: string
schedule?: string
format?: [string]
issues?: number
timeline?: string
_type: ElementType = ElementType.ComicSeries

}
