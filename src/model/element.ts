import {Deserializable} from "./pageResponse";

export enum ElementType {
    Character = "Character",
    Droid = "Droid",
    Default = "Element",
    BookSeries = "BookSeries",
    Book = "Book",
    City = "City",
    ComicBook = "ComicBook",
    ComicSeries = "ComicSeries",
    Constellation = "Constellation",
    Deity = "Deity",
    Disease = "Disease",
    DroidSeries = "DroidSeries",
    IndividualShip = "IndividualShip",
    Lightsaber = "Lightsaber",
    Location = "Location",
    Moon = "Moon",
    Movie = "Movie",
    Nebula = "Nebula",
    Planet = "Planet",
    Region = "Region",
    Sector = "Sector",
    TVEpisode = "TVEpisode",
    TVSeason = "TVSeason",
    TVSeries = "TVSeries",
}

export class SWElement extends Deserializable {
    image?: string
    id: number
    searchScore?: number
    name: string
    wp: string
    entity: ElementType = ElementType.Default

    constructor() {
        super();
        this.name = ""
        this.wp = ""
        this.id = 0
    }

}
