import {SWElement} from "./element";

export class Deserializable {
    deserialize(obj: Object): Deserializable {
        obj && Object.assign(this, obj);
        return this;
    }
}

export type PageResponse = {
    results: [SWElement];
    _links?: {
        first: Link,
        self: Link,
        next: Link,
        last: Link,
    }
    page?: number,
    size?: number,
    totalElements: number,
    totalPages: number,

}

export type Link = {
    href: string
}