import {createTheme} from "@mui/material";
// import Mandalor from "../assets/mandalor.ttf";
// import Aurebesh from "../assets/AurebeshINV_Medium.otf";

export const darkTheme = createTheme({
    palette: {
        mode: 'dark',
        secondary: {
            main: "#212121"
        }
    },
});
