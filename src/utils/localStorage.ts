import {ApplicationState} from "../reducer/state";

export const loadState = () : ApplicationState => {
    try {
        const serializedState = localStorage.getItem('aas_state');
        if (serializedState === null) {
            return {
                darkTheme: window.matchMedia ? window.matchMedia('(prefers-color-scheme: dark)').matches : true
            };
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return {darkTheme: true};
    }
};


export const saveState = (state: any) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('aas_state', serializedState);
    } catch {
        // ignore write errors
    }
};
