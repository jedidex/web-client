// @ts-ignore
import {ElementType} from "../model/element";

export const SERVER: string = "https://pyapi.jedidex.com"


export const supportedElements = [ElementType.Character]