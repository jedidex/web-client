import {useCallback, useState} from "react";
import {SERVER} from "../utils/constants";

export enum Method {
    GET = "get",
    POST = "post",
    PUT = "put",
    DELETE = "delete",
}

const serialize = ((obj: { [key: string]: any }): string => {
    let str = [];
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
})

export const useApi = <T>(method: Method, url: string): [RequestState, (headers?: any, params?: { [key: string]: any }) => Promise<T>] => {

    const [request, setRequest] = useState<RequestState>({isWorking: false})

    const makeRequest = useCallback((
            headers: { [key: string]: any } = {},
            params: Object | null = null): Promise<T> =>
            new Promise<T>((resolve, reject) => {

                let json: string | null = null
                let queryParams: string = ""

                if (params && method !== Method.GET) {
                    json = JSON.stringify(params)
                    if (!('Content-Type' in headers)) {
                        headers['Content-Type'] = "Application/json"
                    }
                }
                else if(params && method === Method.GET){
                    queryParams = `?${serialize(params)}`
                }

                setRequest({isWorking: true})

                const server = SERVER + (SERVER.endsWith("/") ? "" : "/")

                fetch(server + url + queryParams, {
                    headers: headers,
                    body: json,
                    method: method
                })
                    .then(response => {
                            // if success
                            if (response.status < 400)
                                response.json().then((json) => {
                                    resolve(json as T)
                                    setRequest({isWorking: false, statusCode: response.status})
                                })

                            // if error
                            else
                                response.json().then(json => {
                                    reject(json)
                                    setRequest({isWorking: false, statusCode: response.status, error: json.message})
                                })
                        }
                    )
                    .catch((error) => {
                            reject(error)
                            setRequest({isWorking: false, error: error.message})
                        }
                    )


            }),
        [method, url],
    );

    return [
        request,
        makeRequest,
    ];
};

export type RequestState = {
    isWorking: boolean,
    statusCode?: number,
    error?: string
}


