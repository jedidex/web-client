export type ApplicationState = {
    user?: User,
    darkTheme: boolean
}

export type User = {
    name: string,
    surname: string,
    username: string,
}

