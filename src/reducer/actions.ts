import {User} from "./state";

export type Action =
    | { type: 'request' }
    | { type: 'success', results: User }
    | { type: 'failure', error: string };