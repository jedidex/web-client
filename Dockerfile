# Stage 1
FROM node:14 as react-build
WORKDIR /app
COPY . ./
RUN npm install --silent
RUN npm run build

# Stage 2 - the production environment
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /app/build /etc/nginx/html
COPY --from=react-build /app/build /usr/share/nginx/html

WORKDIR /usr/share/nginx/html

# Add bash
RUN apk add --no-cache bash

EXPOSE 80
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
